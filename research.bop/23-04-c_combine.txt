...what would be absolutely killer is combining verbot with bop
such that you can make bop entries that represent a certain list of concepts put together into a sentence, and you look up a bop entry addressed as "what-is bop" that contains dialogue box contents
this is a little different from how entries usually work - usually you tag them with labels like "t. what-is" "t. explain" and you could open the entry by either label, while this would be more of an "AND" path to opening entries.
but the normal way entries work would still be fine for defining synonymous concepts


; to newcomers: this is bop. this is bop right here.
<= 1682645882 scratch   understanding Verbot from scratch
:: cr. 1682658183
:: t. combine

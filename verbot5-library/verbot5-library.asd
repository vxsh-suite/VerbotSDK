;; /Verbot5Library/AssemblyInfo.cs
(defsystem "verbot5-library"
	:author "Valenoern"
		;; originally by Conversive Inc., 2004-2006
	:licence "GPL2"
	:description "experimental port of Verbot 5 functions"
		;; original description: "Library for Verbot 5"
	:version "6.0.0"
	
	:depends-on (
		"alexandria"
		;; examples -
		;; "uiop"  ; to run commands
		)
	:components (
		(:file "c-sharp-toolbox")   ; :conversive.verbot5.c#-toolbox
		))


;; using System.Reflection;
;; using System.Runtime.CompilerServices;

;;; c-sharp-toolbox.lisp - utility functions
;; /Verbot5Library/CSharpToolbox.cs
(defpackage :conversive.verbot5.c#-toolbox
	;;(:shadowing-import-from )
	(:use :common-lisp)
	;; (:local-nicknames (:util :v-refracta.alpm.util))
	(:export
		#:string-table #:string-table-get #:string-table-put #:string-table-p #:make-string-table
		#:job #:job-name #:job-args #:job-result #:job-p #:make-job
))
(in-package :conversive.verbot5.c#-toolbox)

;; using System.Collections;
;; using System.Text;
;; using Microsoft.CSharp;
;; using System.CodeDom.Compiler;
;; using System.Reflection;
;; using System.IO;
;; using System.Threading;
;; using System.Windows.Forms;
;; using System.Collections.Generic;

;; namespace Conversive.Verbot5


;;; string-table  - public class StringTable

(defun string-table-p (table)
	(let (string-key-p hash-test)
	(and  ; this is a string-table if it...
		(hash-table-p table)  ; is a hash table,
		(or  ; has a test appropriate for strings,
			(equal (setq hash-test (hash-table-test table)) 'equal)
			(equal hash-test 'equalp))
		(cond  ; and either...
			((equal (hash-table-count table) 0)  t)  ; has no keys, OR
				;; note: length of hash table appears to be a cached statistic in SBCL
			(t  ; has keys which are only strings
				(maphash
					#'(lambda (key val)
						(setq string-key-p
							(if (stringp key)
								t
								nil)))
					table)
				string-key-p)))
))
(deftype string-table ()
	`(satisfies string-table-p))

(defun string-table-get (key table)
	(let* ((small-key (string-downcase key))
		(value (gethash small-key table)))
	;; LATER: make sure small-key is string?
	(if (null value)
		""
			;; if(base[key] == null)  return (string)base[key];//return null string
		value)))
(defun string-table-put (key value table)
	(setq key (string-downcase key))
	(setf (gethash key table) value))

;; a string-table is really nothing more than a regular hash-table
;; this function exists just to make sure you use an appropriate :test for strings
(defun make-string-table (&key (test 'equal) (size 0))
	;; ensure an appropriate test
	(unless (or (equal test 'equal) (equal test 'equalp))
		(setq test 'equal))
	(make-hash-table
		:test test
		:size size))


;;; job  - public class Job

(defstruct (job)
	(name "")
		;; this.name = "";
	(args nil)
		;; this.args = new List<object>();
	(result nil)
		;; this.result = null;
	
	;; the original Job class had duplicate private and public versions of the fields, where an extra public version would get the private version:
	;;	private object result;
	;;	public object Result {
	;;		get { return this.result; }
	;;		set { this.result = value; }
	;;	}
	;; but all this did not actually add anything, so now Job is an ordinary struct
)

using System.IO;

namespace Conversive.Verbot5
{
	/// Logs errors to a text file or the event registry.
	public class ErrorLogger
	{

		public static void SendToLogFile(string stPath, string stToLog)
		{
			try
			{
				stToLog = DateTime.Now.ToString("s") + ": " + stToLog + "\r\n";
				FileStream fs = new FileStream(stPath, FileMode.Append);
				StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
				sw.Write(stToLog);
				sw.Flush();
				sw.Close();
			}
			catch(Exception e)
			{
				SendToEventLog("Log File Error: " + e.ToString() + "\r\n" + e.StackTrace);
			}
		}

		public static void SendToEventLog(string stErrorMessage)
		{
			string sSource = "Verbot5";
			string sLog = "Application";
			string sEvent = stErrorMessage;

			if (!System.Diagnostics.EventLog.SourceExists(sSource))
				System.Diagnostics.EventLog.CreateEventSource(sSource,sLog);

			System.Diagnostics.EventLog.WriteEntry(sSource, sEvent);
			//System.Diagnostics.EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Warning, 234);
		}
	}//class ErrorLogger
}//namespace Conversive.Verbot5
